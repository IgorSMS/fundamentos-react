import React, { useState } from 'react'
import './Input.css'
// eslint-disable-next-line import/no-anonymous-default-export
export default props => {
  const [valor, setValor] = useState('Inicial')

  function quandoMudar(e){
    setValor(e.target.value)
  }
  return (
    <div className="Input">
      <h2>{valor}</h2>
      <input onChange={quandoMudar} value={valor} />
      <input placeholder="readOnly" readOnly value={valor} />
      <input placeholder="uncontrolled" value={undefined} />
    </div>
  )
}