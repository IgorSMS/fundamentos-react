import React from 'react'

// eslint-disable-next-line import/no-anonymous-default-export
export default props => {

  const numAleatorio = Math.floor(Math.random() * (props.max - props.min)) + props.min;
  return (
    <div>
      Aleatório: {numAleatorio}
    </div>
  )
}