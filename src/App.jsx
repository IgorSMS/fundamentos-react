import React from 'react'


import Primeiro from './components/basicos/Primeiro'
import ComParametro from './components/basicos/ComParametro'
import Fragmento from './components/basicos/Fragmento'
import Aleatorio from './components/basicos/Aleatorio'
import Card from './components/layout/Card'
import './App.css'
import Familia from './components/basicos/Familia'
import FamiliaMembro from './components/basicos/FamiliaMembro'
import ListaAlunos from './components/repeticao/ListaAlunos'
import TabelaProdutos from './components/repeticao/TabelaProdutos'
import ParOuImpar from './components/condicional/ParOuImpar'
import UsuarioInfo from './components/condicional/UsuarioInfo'
import DiretaPai from './components/comunicacao/DiretaPai'
import IndiretaPai from './components/comunicacao/IndiretaPai'
import Input from './components/formulario/Input'
import Contador from './components/contador/Contador'
import Mega from './components/mega/Mega'
// eslint-disable-next-line import/no-anonymous-default-export
export default _ => {
  function getRandomColor() {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
  return (

    <div className="App">
      <h1>Fundamentos React</h1>

      <div className="Cards">
        <Card titulo="#14 - Desafio Mega" color={getRandomColor()}>
          <Mega></Mega>
        </Card>
        
        <Card titulo="#13 - Contador" color={getRandomColor()}>
          <Contador numeroInicial={10}></Contador>
        </Card>

        <Card titulo="#12 - Componente Controlado (Input)" color={getRandomColor()}>
          <Input></Input>
        </Card>
        
        <Card titulo="#11 - Comunicação Indireta" color={getRandomColor()}>
          <IndiretaPai></IndiretaPai>
        </Card>
        
        <Card titulo="#10 - Comunicação Direta" color={getRandomColor()}>
          <DiretaPai></DiretaPai>
        </Card>

        <Card titulo="#09 - Renderização Condicional-2" color={getRandomColor()}>
          <UsuarioInfo usuario={{ nome: 'Igor' }}></UsuarioInfo>
          <UsuarioInfo usuario={{ email: 'Igor@email.com' }}></UsuarioInfo>
          <UsuarioInfo></UsuarioInfo>
        </Card>

        <Card titulo="#08 - Renderização Condicional" color={getRandomColor()}>
          <ParOuImpar numero={11} ></ParOuImpar>
        </Card>

        <Card titulo="#07 - Desafio Tabela" color={getRandomColor()}>
          <TabelaProdutos></TabelaProdutos>
        </Card>

        <Card titulo="#06 - Repetição" color={getRandomColor()}>
          <ListaAlunos></ListaAlunos>
        </Card>

        <Card titulo="#05 - Componente com Filhos" color={getRandomColor()}>
          <Familia sobrenome="React">
            <FamiliaMembro nome="Igor"></FamiliaMembro>
            <FamiliaMembro nome="Siqueira"></FamiliaMembro>
            <FamiliaMembro nome="Marques"></FamiliaMembro>

          </Familia>
        </Card>

        <Card titulo="#04 - Desafio Aleatório" color={getRandomColor()}>
          <Aleatorio min={5} max={40}></Aleatorio>
        </Card>

        <Card titulo="#03 - Fragmento" color={getRandomColor()}>
          <Fragmento />
        </Card>

        <Card titulo="#02 - Com Parâmetros" color={getRandomColor()}>
          <ComParametro
            titulo="Status Aluno"
            aluno="Igor"
            nota={9.9}>
          </ComParametro>
        </Card>

        <Card titulo="#01 - Primeiro Componente" color={getRandomColor()}>
          <Primeiro></Primeiro>
        </Card>
      </div>
    </div>
  )
}
