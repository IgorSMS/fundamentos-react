import React from 'react'
import produtos from '../../data/produtos'
// eslint-disable-next-line import/no-anonymous-default-export
export default props => {
  const lineTable = produtos.map((produto, index) => {
    return (
      <tr key={index}>
        {Object.values(produto).map((atributo, indexAtributo) => {
          return (
            <td key={indexAtributo}>{atributo}</td>
          )
        })}
      </tr>
    )
  })
  return (
    <table>
      <thead>
        <tr>
            <th>ID</th>
            <th>Produto</th>
            <th>Preço</th>
        </tr>
      </thead>
      <tbody>
        {lineTable}
      </tbody>
    </table>
  )
}