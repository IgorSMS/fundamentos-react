import React from 'react'
import If from './If'
// eslint-disable-next-line import/no-anonymous-default-export
export default props => {
  return (
    <div>
      <If test={props.usuario?.nome}>
        Seja bem vindo 
        <strong> {props.usuario?.nome}</strong>
      </If>
      <If test={!props.usuario?.nome}>
        Seja bem vindo 
        <strong> Sem nome</strong>
      </If>
    </div>
  )
}