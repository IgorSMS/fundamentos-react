import React, { Component } from 'react'
import Botoes from './Botoes'
import Display from './Display'
import PassoForm from './PassoForm'

class Contador extends Component {
  state = {
    numero: this.props.numeroInicial || 0,
    passo: this.props.passoInicial || 5
  }

  // constructor(props) {
  //   super(props)
  //   this.state = {
  //     numero: this.props.numeroInicial
  //   }
  // }

  inc = () => {
    this.setState({
      numero: this.state.numero + this.state.passo
    })
  }
  dec = () => {
    this.setState({
      numero: this.state.numero - this.state.passo
    })
  }

  setPasso = (e) => {
    this.setState({
      passo: +e.target.value
    })
  }

  render() {
    return (
      <div>
        <Display numero={this.state.numero}></Display>
        <PassoForm changeStatePasso={this.setPasso} passo={this.state.passo}></PassoForm>
        <Botoes inc={this.inc} dec={this.dec}></Botoes>
      </div>
    )
  }
}

export default Contador