import React from 'react'
import DiretaFilho from './DiretaFilho'

// eslint-disable-next-line import/no-anonymous-default-export
export default props => {
  return (
    <div>
      <DiretaFilho texto="Filho 1" numero={15} bool={false}></DiretaFilho>
      <DiretaFilho texto="Filho 2" numero={20} bool={true}></DiretaFilho>
    </div>
  )
}