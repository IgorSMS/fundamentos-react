import React, { Component } from 'react'
import './Mega.css'
export default class Mega extends Component {

  state = {
    numeros: [],
    quantidade: 6
  }

  gerarNumerosAleatorios = _ => {
    let numeros = []
    const min = 1
    const max = 61
    while (numeros.length < this.state.quantidade){
      let numero = Math.floor(Math.random() * (max - min)) + min;
      if (!numeros.some(num => numero === num))
        numeros.push(numero)
    }
    this.setState({ numeros: numeros.sort((n1, n2) => n1 - n2) })
  }

  render (){
    return (
      <div>
        <h2>Números da Mega</h2>
        <div className="Mega_numeros">
          <div className="Mega_container-numeros">
            {this.state.numeros.map(content => {
              return (
                <div className="Mega_span" key={content}>{content}</div>
              )
            })}
          </div>
          <input min='6' max='15' onChange={e => this.setState({quantidade: +e.target.value})} value={this.state.quantidade} className="Mega_input" type="number"/>
          <button className="Mega_button" onClick={this.gerarNumerosAleatorios}>Gerar Números</button>
        </div>
      </div>
    )
  }
}
