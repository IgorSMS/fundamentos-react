import React, { useState } from 'react'
import IndiretaFilho from './IndiretaFilho'

// eslint-disable-next-line import/no-anonymous-default-export
export default props => {
  const [nome, setNome] = useState("?")
  let [idade, setIdade] = useState(0)
  let [nerd, setNerd] = useState(false)

  function fornecerInformacoes(nomeParam, idadeParam, nerdParam){
    setNome(nomeParam)
    setIdade(idadeParam)
    setNerd(nerdParam)
  }

  return (
    <div>
      <div>
        <span>{nome} </span>
        <span><strong>{idade}</strong> </span>
        <span>{nerd ? 'Verdadeiro' : 'Falso'}</span>
      </div>
      <IndiretaFilho quandoClicar={fornecerInformacoes}></IndiretaFilho>
    </div>
  )
}