import React from 'react'
import "./Card.css"
// eslint-disable-next-line import/no-anonymous-default-export
export default props => {
  const cardStyle = {
    backgroundColor: props.color || "#F00",
    borderColor: props.color || "#F00"
  }
  return (
    <div className="Card" style={cardStyle}>
      <h3 className="Title">{props.titulo}</h3>
      <div className="Content">{props.children}</div>
    </div>
  )
}