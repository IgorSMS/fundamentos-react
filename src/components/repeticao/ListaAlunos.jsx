import React from 'react'
import alunos from '../../data/alunos'
// eslint-disable-next-line import/no-anonymous-default-export
export default props => {
  const lis = alunos.map((aluno, index) => {
    return (
      <li key={index}>
        {aluno.nome} - {aluno.nota}
      </li>
    )
  })
  return (
    <div>
      <ol>
        {/* <li>Igor - 9.7</li>
        <li>Siqueira - 9.9</li>
        <li>Marques - 8.9</li> */}
        {lis}
      </ol>
    </div>
  )
}