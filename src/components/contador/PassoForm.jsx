import React from 'react'

// eslint-disable-next-line import/no-anonymous-default-export
export default props => {
  return (
    <div>
    <label htmlFor="">Passo: </label>
    <input 
      id="passo-input" 
      type="number" 
      onChange={props.changeStatePasso} 
      value={props.passo}/>
    </div>
  )
}