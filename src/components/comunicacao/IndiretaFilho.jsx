import React from 'react'

// eslint-disable-next-line import/no-anonymous-default-export
export default props => {
  const min = 50
  const max = 70
  const gerarIdade = () => {
    return Math.floor(Math.random() * (max - min)) + min;
  }
  return (
    <div>
      <div>
        Filho
      </div>
      <button onClick={
        _ => props.quandoClicar('João', gerarIdade, true)
      }>Fornecer informações</button>
    </div>
  )
}